# WIFI_ENSIBS Connect

Auto-connect to the fortinet authentication

## Installation

Clone this repository, install the requirements and run the script when you are connected to the Wi-Fi network "WIFI_ENSIBS".

```bash
git clone https://gitlab.com/thbzzz/wifi-ensibs-connect
pip install -r requirements.txt --user
```

## Usage

First, create a `.env` file in the Python script directory, and fill it like the following, adapting the values to yours:

```
USERNAME="MarotL"
PASSWORD="yugioh123"
```

Then run the script.

```bash
python3 wifi-ensibs-connect.py
```

That's it, you should be authenticated.

## License
[MIT](https://choosealicense.com/licenses/mit/)