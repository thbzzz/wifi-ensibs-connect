#!/usr/bin/env python3

from bs4 import BeautifulSoup
from urllib.parse import urlparse
from os import getenv
from os.path import join, dirname
from dotenv import load_dotenv
from urllib3 import disable_warnings
import requests

REDIRECT_URL = "https://duckduckgo.com"
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36"

# Read USERNAME and PASSWORD from .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
USERNAME = getenv("USERNAME")
PASSWORD = getenv("PASSWORD")

# Disable useless TLS warnings
disable_warnings()

# Get the gateway IP
html = requests.get(
    "https://example.com", allow_redirects=False, verify=False
).text
soup = BeautifulSoup(html, "html.parser")
url = urlparse(soup.find("a")["href"])

# First request to GET the license terms page
headers = {"User-Agent": USER_AGENT}
req = requests.get(url.geturl(), headers=headers, verify=False)

# Second request (POST) to accept de license terms
headers = {"User-Agent": USER_AGENT, "Referer": url.geturl()}
data = {
    "4Tredir": REDIRECT_URL,
    "magic": url.query,
    "answer": "1"
}
req = requests.post(
    "{}://{}/".format(url.scheme, url.netloc),
    headers=headers,
    data=data,
    verify=False
)

# Third request (POST) to submit the login form
headers = {"User-Agent": USER_AGENT}
data = {
    "4Tredir": REDIRECT_URL,
    "magic": url.query,
    "username": USERNAME,
    "password": PASSWORD    
}
req = requests.post(
    "{}://{}/".format(url.scheme, url.netloc),
    headers=headers,
    data=data,
    verify=False
)

print("OK")